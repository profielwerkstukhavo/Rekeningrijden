const JSONCrypt = require('@jorveld/jsoncrypt');

console.log("crypt test");

var toBeEncryptedObject = { name: "Nolan", gender: "male", age: 45 }    

console.log("create RSA Keys"); 
RSAPair = JSONCrypt.getRSAPair(); 
console.log("RSAPair:"); 
console.log(RSAPair);

console.log("AsyncCrypt"); 
asyncCryptResult=JSONCrypt.cryptAsync(toBeEncryptedObject, RSAPair.publicKey); 
console.log("asyncCryptResult="+asyncCryptResult);

console.log("AsyncUncrypt"); asyncDecryptResult=JSONCrypt.deCryptAsync(asyncCryptResult, RSAPair.privateKey); 
console.log("asyncDecryptResult="+asyncDecryptResult);
