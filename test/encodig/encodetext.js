const JSONCrypt = require('@jorveld/jsoncrypt')
const dbsysteem = require('../../rekeningrijden/dbsysteem/dbsysteem.js');


var persoonlijkeSynchroneEncryptieSleutel=JSONCrypt.createSyncEncryptionKeyAndIv();

console.log(persoonlijkeSynchroneEncryptieSleutel);
console.log("The Type of the IV: "+typeof(persoonlijkeSynchroneEncryptieSleutel.iv));
console.log("In IV in hex string: "+ persoonlijkeSynchroneEncryptieSleutel.iv.toString('hex'));

var theObject = {
    "naam":"jurriaan"
}


theCrypted=JSONCrypt.cryptSync(theObject, persoonlijkeSynchroneEncryptieSleutel.key, persoonlijkeSynchroneEncryptieSleutel.iv)


console.log("theCrypted:");
console.log(theCrypted);

var ivString=persoonlijkeSynchroneEncryptieSleutel.iv.toString('hex');
var keyString=persoonlijkeSynchroneEncryptieSleutel.key.toString('hex');

console.log("keyString:");
console.log(keyString);
console.log("ivString:");
console.log(ivString);

keyReverse=Buffer.from(keyString,"hex");
ivReverse=Buffer.from(ivString,"hex");

console.log("keyReverse:");
console.log(keyReverse);

console.log("ivReverse:");
console.log(ivReverse);


decrypted=JSONCrypt.deCryptSync (theCrypted.encryptedContent, keyReverse, ivReverse);

console.log("decrypted:");
console.log(decrypted);

var persoonlijkeSynchroneEncryptieSleutelVoorOpslag = {
    key:keyString,
    iv:ivString
};



promiseGeschrevenSluetel=dbsysteem.put("persoonlijkeSynchroneEncryptieSleutelDb", "X",persoonlijkeSynchroneEncryptieSleutelVoorOpslag);

promiseGeschrevenSluetel.then(function(result) {
    promiseOpgehaaldeKey=dbsysteem.get("persoonlijkeSynchroneEncryptieSleutelDb","X");
    promiseOpgehaaldeKey.then(function(opgehaaldeEncryptionKey)
    {
        console.log("opgehaaldeEncryptionKey");
        console.log(opgehaaldeEncryptionKey);
        var opgehaaldeKey=Buffer.from(opgehaaldeEncryptionKey.key,"hex");
        var opgehaaldeIv=Buffer.from(opgehaaldeEncryptionKey.iv,"hex");
        console.log("opgehaaldeKey");
        console.log(opgehaaldeKey);
        console.log("opgehaaldeIv");
        console.log(opgehaaldeIv);
        opgehaaldDecrypted=JSONCrypt.deCryptSync (theCrypted.encryptedContent, opgehaaldeKey, opgehaaldeIv);
        console.log("opgehaaldDecrypted:");
        console.log(opgehaaldDecrypted);
    });
});



