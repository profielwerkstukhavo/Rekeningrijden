const { Level } = require('level')
const db = new Level('./db', { valueEncoding: 'json' })

async function main () {

  await db.put('a', 1)

 
  await db.batch([{ type: 'put', key: 'b', value: 2 }])

  
  const value = await db.get('a')

  
  for await (const [key, value] of db.iterator({ gt: 'a' })) {
    console.log(value) // 2
  }
}

main()