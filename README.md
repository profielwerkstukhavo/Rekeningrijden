# Profielwerkstuk: Minder files door rekeningrijden


Gemaakt door: Ricardo Javier Pereira Rodriguez (H5a) en Jesper Brandsma (H5b)
School: Pieter Nieuwenland College  


## Inhoud
Deze repository bevat code die Ricardo xx en Jesper Brandsma is gemaakt als onderdeel van hun profielwerkstuk 'Minder files door rekeningrijden'.
Het profielwerkstuk in de root van deze repository gezet.


## Testen/Runnen
In deze repository staan twee hoofd directories:
- test. In de test directory staan kleine programmas die zijn gemaakt om de technolgie die in het project is gebruikt te testen. De testen zijn beschreven Hoofdstuk 3, Aanpak, Stap 1: Installeren van software en experimenteren met benodigde programma's.
Om de testen te herhalen moet het volgende gedaan worden:
Stap 1: Installeren van nodejs en npm. Hoe dat moet staat in het werkstuk in de paragraaf die hierboven wordt genoemd.
Stao 2: Clone (download) the code.
Stap 3: Ga naar de test directory die je wilt uitproberen en run npm install. Hiermee worden alle libraries (Javascript modules) geladen. 
Stap 4: Run 'node <testprogramma.js>'.

- rekeningrijden. In de rekeningrijden directory staat de code die we voor het project hebben gemaakt. Iedere directory bevat de code voor een deelsysteem (module). Alle modules zijn beschreven in 
Hoofdstuk 3, 
- Paragraaf Ontwerp op hoofdlijnen
- Paragraaf Detail ontwerp
Daarnaast vind je de volgende directies in rekeningrijden:
- certificaten. Hier staan de digitale certificaten die gebruikt worden in het project.
- db. Hier staan de databases die gebruikt worden in het project. Deze directory hoeft niet gecloned te worden. De inhoud wordt automatisch aangemaakt als de programma's runnen.
- dbsysteem. Hier staat een klein javascript programma in waarmee in de database wordt geschreven en gelezen.
Om de code te controleren en gebruiken kunnen de testen herhaald worden die beschreven zijn in Hoofdstuk 3, Aanpak, Stap 3: testen moet je het volgende doen:
Stap 1: Installeren van nodejs en npm. Hoe dat moet staat in het werkstuk in de paragraaf die hierboven wordt genoemd.
Stao 2: Clone (download) the code.
Stap 3: Ga naar de test directory met de module die je wilt testen. run npm install. Hiermee worden alle libraries (Javascript modules) geladen. 
Stap 4: Run 'node test.js'.

Opmerkingen: 
- De module dbsysteem heeft geen testen.
- De mobieleTelefoon heeft geen aparte test.js. Hier kun je runnen 'node mobieleTelefoon.js'.
