const fs = require('fs');
const JSONCrypt = require('@jorveld/jsoncrypt');
const { Level } = require('level')

const publicKeyKostenBerkenSysteem=fs.readFileSync("../certificaten/publicKeyKostenBerekenSysteem.key"); 
const publicKeyLoginSysteem=fs.readFileSync("../certificaten/publicKeylogin.key");

const rekeningenDb = new Level('../db/rekeningen', { valueEncoding: 'json' })


exports.geefKostenOp = function (persoonlijkToegangsbewijs, kostenSpecificatie)
{
  /* invoer:
    persoonlijkToegangsbewijs 
    {
      onderekenaar,
      uitgaveTijd,
      bsn,   
      kenteken,
      handtekening (van het inlogSysteem)
  }

  kostenSpecificatie
  {
    kosten,
    volgnummer,
    tijdsopgave,
    handtekening (van het KostenBerekenSystem)
  }
  */


  // Stap 1: Controleer het persoonlijke toegangsbewijs. Als de handtekening niet klopt, of de gebruiker is onbekend of de handtekening is niet meer geldig, dan wordt de opgave niet verwerkt.
  if (controleerPersoonsGebondenToegangsbewijs(persoonlijkToegangsbewijs)==false)
  {
    return "Het persoonsgebonden toegangsbewijs is niet geldig"; // er is iets mis met het toegangsbewijs. Geef een fout terug.
  }

  // Stap 2: Controleert de handtekening van de KostenSpecificatie. Als deze niet van de KostenBereken module komt, dan zal de opgave niet worden verwerkt.
  if (controleerHandtekening(kostenSpecificatie, publicKeyKostenBerkenSysteem)==false)
  {
    return "De kosten specificatie is niet geldig";
  }

  // Stap 3: Controleert of de tijdsopgave van de kosten specificatie. Er moet iedere minuut een kostenopgave gemaakt worden.
  if (controleerTijdVanKostenSpecificatie(kostenSpecificatie)==false)
  {
    return "Tijdsfout voor kosten specificatie";
  }

  // Stap 4: Controleert het volgnummer. De telefoon mag geen berichten (kostenopgaves) weglaten.
  if (controleerVolgnummerVanKostenSpecificatie(kostenSpecificatie)==false)
  {
    return "Volgnummer fout voor kosten specificatie";
  }

  // Stap 5: Voeg kosten aan rekening van gebruiker toe.
  // var huidigeKosten = voegKostenToe(persoonlijkToegangsbewijs.bsn,kostenSpecificatie);
  
  voegKostenToe(persoonlijkToegangsbewijs.bsn,kostenSpecificatie);

  return "OK";
}


function controleerPersoonsGebondenToegangsbewijs(invoerPersoonsGebondenToegangsbewijs)
{
  var kopiePersoonsGebondenToegangsbewijs=JSON.parse(JSON.stringify(invoerPersoonsGebondenToegangsbewijs)); // hier wordt een kopie gemaakt van het persoonsGebondenToegangsbewijs. Dat moet omdat we de handtekening er uit moeten halen.
  delete kopiePersoonsGebondenToegangsbewijs.handtekening; // hier wordt de handtekening uit de kopie gehaald. Dat is nodig omdat we de hantekening moeten controlen van het toegangsbewijs zonder de handtekening
  resultaatPersoonsGebondenToegangsbewijs = JSONCrypt.verifySignature(invoerPersoonsGebondenToegangsbewijs.handtekening, publicKeyLoginSysteem,kopiePersoonsGebondenToegangsbewijs);
  if (resultaatPersoonsGebondenToegangsbewijs==false)
  {
    return false; // de handtekening op het anonieme toegangsbewijs klopt niet.
  }
  if (invoerPersoonsGebondenToegangsbewijs.onderekenaar!="Rekening Rijden Nederland")
  {
    return false; // het toegangsbewijs is niet uitgegeven door 'Rekening Rijden Nederland'. Het is dus vervalst.
  }
  if (Date.now()>invoerPersoonsGebondenToegangsbewijs.geldigtot)
  {
    return false; // het toegangsbewijs is niet meer geldig. 
  }
  return true; // Alles is goed met het toegangsbewijs.
}


function controleerHandtekening(invoerJSON, publicKey)
{
  var kopieInvoerJSON=JSON.parse(JSON.stringify(invoerJSON)); // hier wordt een kopie gemaakt van de JSON. Dat moet omdat we de handtekening er uit moeten halen.
  delete kopieInvoerJSON.handtekening; // hier wordt de handtekening uit de kopie gehaald. Dat is nodig omdat we de hantekening moeten controlen van het JSON zonder de handtekening
  JSONValide = JSONCrypt.verifySignature(invoerJSON.handtekening, publicKey,kopieInvoerJSON);
  return JSONValide;
}

function controleerTijdVanKostenSpecificatie(kostenSpecificatie)
{
    // In een definitieve oplossing zal iedere kostenspecificatie worden opgeslagen. 
    // Wanneer een nieuwe kostenspecificatie binnen komt wordt de laaste kosten specificatie opgehaald.
    // Gekeken wordt of er niet te veel tijd is verspreken tussen de nieuwe en de eerdere specificatie,
    // Omdat dit een prototype is, wordt deze controle hier niet uitgevoerd. Er wordt altijd true teruggegeven.
    return true; 
}

function controleerVolgnummerVanKostenSpecificatie(kostenSpecificatie)
{
    // In een definitieve oplossing zal iedere kostenspecificatie worden opgeslagen. 
    // Wanneer een nieuwe kostenspecificatie binnen komt wordt de laaste kosten specificatie opgehaald.
    // Gekeken wordt of de volgnummers van de nieuwe en de eerdere specificatie op elkaar volgen.
    // Omdat dit een prototype is, wordt deze controle hier niet uitgevoerd. Er wordt altijd true teruggegeven.
    return true;
}

async function voegKostenToe(bsn, kostenSpecificatie)
{
  // In deze functie moeten de kosten in een database opgeslagen worden.

  try {
    var huidigeKosten = await rekeningenDb.get(bsn);
  } catch(e) {
    // er is een error gebeurd. Dat komt omdat de bsn nog niet in de db staat.
    await rekeningenDb.put(bsn, 0);
    huidigeKosten=0;
  }
  huidigeKosten=huidigeKosten+kostenSpecificatie.kosten;
  await rekeningenDb.put(bsn, huidigeKosten);
}

