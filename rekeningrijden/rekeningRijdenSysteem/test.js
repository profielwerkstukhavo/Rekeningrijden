const JSONCrypt = require('@jorveld/jsoncrypt');
const kostenBerekenSysteem = require('../kostenBerekenSysteem/kostenBerekenSysteem.js');
const inlog = require('../inlogSysteem/inlogSysteem.js');
const rekeningRijdenSysteem = require('./rekeningRijdenSysteem.js');
const fs = require('fs');

const publicKeyKostenBerekenSysteem=fs.readFileSync("../certificaten/publicKeyKostenBerekenSysteem.key"); 

console.log("Testen RekeningRijdenSysteem systeem");

//RekeningRijdenSysteemTest01
console.log("RekeningRijdenSysteemTest01");

var logingegevens = 
{
    gebruikersnaam: "Jesper Brandsma",
    wachtwoord: "password",
    kenteken: "AF-93-UT"
};

var startCoordinaat = {"Lon":"4.9066792","Lat":"52.3735073","tijd":Date.now()};

var inlogPromise=inlog.login(logingegevens);

inlogPromise.then(function(toegangsbewijzen)
{
    var ondertekendCoordinaat=kostenBerekenSysteem.zetStartCoordinaat(toegangsbewijzen.anoniemToegangsbewijs,startCoordinaat);
    var eindCoordinaat = {"Lon":"4.9064437","Lat":"52.3736282","tijd":Date.now()};
    var kostenResultaat=kostenBerekenSysteem.berekenKosten(toegangsbewijzen.anoniemToegangsbewijs, ondertekendCoordinaat, eindCoordinaat);
    var inputRekeningRijdenSysteem = {
      persoonsGebondenToegangsbewijs: toegangsbewijzen.persoonsgebondenToegangsbewijs,
      kostenSpecificatie:kostenResultaat.kostenSpecificatie
    }

    console.log("toegangsbewijzen");
    console.log(toegangsbewijzen);
    console.log("kostenResultaat");
    console.log(kostenResultaat);
    console.log("inputRekeningRijdenSysteem");
    console.log(inputRekeningRijdenSysteem);
    rekeningRijdenSysteemOutput=rekeningRijdenSysteem.geefKostenOp(inputRekeningRijdenSysteem.persoonsGebondenToegangsbewijs,inputRekeningRijdenSysteem.kostenSpecificatie);
    console.log("rekeningRijdenSysteemOutput");
    console.log(rekeningRijdenSysteemOutput);

});





