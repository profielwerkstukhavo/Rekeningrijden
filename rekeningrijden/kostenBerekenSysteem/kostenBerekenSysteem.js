const fs = require('fs');
const JSONCrypt = require('@jorveld/jsoncrypt');

const privateKeyKostenBerkenSysteem=fs.readFileSync("../certificaten/privateKeyKostenBerekenSysteem.key");
const publicKeyKostenBerkenSysteem=fs.readFileSync("../certificaten/publicKeyKostenBerekenSysteem.key"); 
const publicKeyLoginSysteem=fs.readFileSync("../certificaten/publicKeylogin.key");

const maxTijdTussenMeldingen=2*60*1000; // er mag niet meer dan 2 minuten zitten tussen het opgeven van coordinaten

exports.zetStartCoordinaat = function (anoniemeToegangsbewijs, startCoordinaat)
{
  /* invoer:
    anoniemeToegangsbewijs
    {
      ondertekenaar
      geldigtot
      handtekening
    }

    startCoordinaat
    {
      Lon
      Lat
      tijd
    }

  */

  var onderekendCoordinaat = {
    coordinaat: startCoordinaat,
    volgnummer: 1 // dit is het eerste coordinaat van de rit 
    // handtekening
  }

  // Stap 1: Controleer het anoniemeToegangsbewijs
  if (controleerAnoniemeToegangsbewijs(anoniemeToegangsbewijs)==false)
  {
    return "Het anonieme toegangsbewijs is niet geldig"; // er is iets mis met het toegangsbewijs. Geef een fout terug.
  }

  // Stap 2: Maak een handteking
  handtekening=JSONCrypt.sign(onderekendCoordinaat, privateKeyKostenBerkenSysteem);
  onderekendCoordinaat.handtekening=handtekening;
  return onderekendCoordinaat;
}

exports.berekenKosten = function (anoniemeToegangsbewijs, ondertekendStartCoordinaat, eindCoordinaat)
{
/* invoer:
    anoniemeToegangsbewijs
    {
      ondertekenaar
      geldigtot
      handtekening
    }

    ondertekendStartCoordinaat =
    {
      coordinaat: {
        Lon
        Lat
        tijd
      }
      volgnummer
    }

    eindCoordinaat
    {
      Lon
      Lat
      tijd
    }

  */


  var kostenSpecificatie = {
    kosten: 0,
    volgnummer: 0,
    tijdsopgave: Date.now()
    // handtekening
  }

  var gebiedsVerklaring = {
    cameraID:0,
    tijd: Date.now()
    // handtekening
  }

  var ondertekendEindCoordinaat = {
    coordinaat: eindCoordinaat,
    volgnummer: ondertekendStartCoordinaat.volgnummer+1 // maak het volgnummer één hoger 
    // handtekening
  }

  // Stap 1: De functie controleert het anonieme toegangsbewijs. Alleen iemand die van tevoren is ingelogd en een toegangsbewijs heeft dat nog geldig is, kan de functie gebruiken.

  if (controleerAnoniemeToegangsbewijs(anoniemeToegangsbewijs)==false)
  {
    return "Het anonieme toegangsbewijs is niet geldig"; // er is iets mis met het toegangsbewijs. Geef een fout terug.
  }

  // Stap 2: De functie controleert het ondertekende start coördinaat. Alleen als dit coördinaat een juiste handtekening heeft wordt het toegelaten. Hierdoor kan de telefoon het vorige coördinaat niet vervalsen.

  if (controleerHandtekeningCoordinaat(ondertekendStartCoordinaat)==false)
  {
    return "De handtekening op het ondertekend Start Coordinaat is niet geldig"; // er is iets mis met het het start cooridnaat.  Geef een fout terug.
  }

  // stap 3: De functie controleert of er niet te veel tijd zit tussen het eerste en het tweede coördinaat. De telefoon moet één keer per minuut een coördinaat doorgeven. Als de telefoon dat niet doet, probeert de telefoon misschien fraude te plegen. 
  if (Date.now()-ondertekendStartCoordinaat.tijd>maxTijdTussenMeldingen)
  {
    return "Er zit te veel tijd tussen het start- en het eind coordinaat";
  }

  // stap 4: De functie berekent de kosten (door het verschil tussen de twee coördinaten te berekenen) en ondertekent de kosten
  var kosten=berekenKosten(ondertekendStartCoordinaat.coordinaat, eindCoordinaat)
  kostenSpecificatie.kosten=kosten;
  kostenSpecificatie.volgnummer=ondertekendStartCoordinaat.volgnummer;
  handtekening=JSONCrypt.sign(kostenSpecificatie, privateKeyKostenBerkenSysteem);
  kostenSpecificatie.handtekening=handtekening;

  // stap 5: De functie controleert of je in de buurt van een controle camera bent. Als dat zo is wordt een GebiedsVerklaring gemaakt. Hiermee kan de camera later controleren dat je geen fraude pleegt. 
  var cameraNummer=cameraInDeBuurt(eindCoordinaat);
  gebiedsVerklaring.cameraID=cameraNummer;
  handtekening=JSONCrypt.sign(gebiedsVerklaring, privateKeyKostenBerkenSysteem);
  gebiedsVerklaring.handtekening=handtekening;

  // stap 6: De functie maakt een ondertekende versie van de eind coördinaat aan. Dit coördinaat zal de volgende keer als input worden meegegeven.
  handtekening=JSONCrypt.sign(ondertekendEindCoordinaat, privateKeyKostenBerkenSysteem);
  ondertekendEindCoordinaat.handtekening=handtekening;

  uitvoer = {
    kostenSpecificatie: kostenSpecificatie,
    gebiedsVerklaring: gebiedsVerklaring,
    ondertekendEindCoordinaat: ondertekendEindCoordinaat
  }

  if (cameraNummer!=0) // alleen als een camera gevonden is in de buurt wordt de gebiedsVerklaring toegevoegd.
  {
    uitvoer.gebiedsVerklaring=gebiedsVerklaring;
  }

  handtekening=JSONCrypt.sign(kostenSpecificatie, privateKeyKostenBerkenSysteem);

  return uitvoer;
}


function controleerAnoniemeToegangsbewijs(invoerAnoniemeToegangsbewijs)
{
  var kopieAnoniemeToegangsbewijs=JSON.parse(JSON.stringify(invoerAnoniemeToegangsbewijs)); // hier wordt een kopie gemaakt van het anoniemeToegangsbewijs. Dat moet omdat we de handtekening er uit moeten halen.
  delete kopieAnoniemeToegangsbewijs.handtekening; // hier wordt de handtekening uit de kopie gehaald. Dat is nodig omdat we de hantekening moeten controlen van het toegangsbewijs zonder de handtekening
  resultaatAnoniemeToegangsbewijs = JSONCrypt.verifySignature(invoerAnoniemeToegangsbewijs.handtekening, publicKeyLoginSysteem,kopieAnoniemeToegangsbewijs);
  if (resultaatAnoniemeToegangsbewijs==false)
  {
    return false; // de handtekening op het anonieme toegangsbewijs klopt niet.
  }
  if (invoerAnoniemeToegangsbewijs.onderekenaar!="Rekening Rijden Nederland")
  {
    return false; // het toegangsbewijs is niet uitgegeven door 'Rekening Rijden Nederland'. Het is dus vervalst.
  }
  if (Date.now()>invoerAnoniemeToegangsbewijs.geldigtot)
  {
    return false; // het toegangsbewijs is niet meer geldig. 
  }
  return true; // Alles is goed met het toegangsbewijs.
}

function controleerHandtekeningCoordinaat(invoerCoordinaat)
{
  var kopieCoordinaat=JSON.parse(JSON.stringify(invoerCoordinaat)); // hier wordt een kopie gemaakt van het coordinaat. Dat moet omdat we de handtekening er uit moeten halen.
  delete kopieCoordinaat.handtekening; // hier wordt de handtekening uit de kopie gehaald. Dat is nodig omdat we de hantekening moeten controlen van het toegangsbewijs zonder de handtekening
  resultaatCoordinaat = JSONCrypt.verifySignature(invoerCoordinaat.handtekening, publicKeyKostenBerkenSysteem,kopieCoordinaat);
  return resultaatCoordinaat;
}

function berekenKosten(startCoordinaat, eindCoordinaat)
{
  // Om het niet te ingewikkeld te maken, wordt hier één prijs per kilometer gebruikt. In het echt kan hier rekening gehouden worden met plaats en tijd.
  // Er kan bijvoorbeeld gekeken worden of het eindCoordinaat in een milieuzone ligt. Ook kan met de tijd in het coordinaat worden gekeken of de auto in de spits rijdt.
  const prijsPerKm = 0.28; 


  let startingLat = degreesToRadians(startCoordinaat.Lat); 
  let startingLong = degreesToRadians(startCoordinaat.Lon); 
  let destinationLat = degreesToRadians(eindCoordinaat.Lat); 
  let destinationLong = degreesToRadians(eindCoordinaat.Lon); 

  let radius = 6371;

  let distanceInKilometers = Math.acos(
    Math.sin(startingLat) * Math.sin(destinationLat) +
    Math.cos(startingLat) * Math.cos(destinationLat) *
    Math.cos(startingLong - destinationLong)
  ) * radius;

  return distanceInKilometers*prijsPerKm;
}

function degreesToRadians(degrees) {
  var radians = (degrees * Math.PI) / 180;
  return radians;
}

function cameraInDeBuurt(coordinaat)
{
  // Om dat dit een proefopstelling is wordt voor ieder coordinaat 67 terugggegeven (het nummer van de camera waar de auto dicht bij is)
  // In het echt zal er steeds een controle op een database gedaan worden om te kijken of de auto dicht bij een specifieke camera is. 
  // Alleen als dat zo is zal het camera nummer worden teruggeven. Anders zal een 0 worden teruggegeven.
  return 67;
}

 