const JSONCrypt = require('@jorveld/jsoncrypt');
const kostenBerekenSysteem = require('./kostenBerekenSysteem.js');
const inlog = require('../inlogSysteem/inlogSysteem.js');
const fs = require('fs');

const publicKeyKostenBerekenSysteem=fs.readFileSync("../certificaten/publicKeyKostenBerekenSysteem.key"); 

console.log("Testen kostenBerekenSysteem systeem");

//KostenBerekenSysteemTest01
console.log("KostenBerekenSysteemTest01");

var logingegevens = 
{
    gebruikersnaam: "Jesper Brandsma",
    wachtwoord: "password",
    kenteken: "AF-93-UT"
};

var startCoordinaat = {"Lon":"4.9066792","Lat":"52.3735073","tijd":Date.now()};

// Het inloggen kan langer duren. Daarom geeft de inlog module een promise terug. Met een .then kan gekeken worden wanneer de promise klaar is.
inlogPromise=inlog.login(logingegevens);

inlogPromise.then(function(toegangsbewijzen) {
    ondertekendCoordinaat=kostenBerekenSysteem.zetStartCoordinaat(toegangsbewijzen.anoniemToegangsbewijs,startCoordinaat);
    console.log("ondertekendCoordinaat:");
    console.log(ondertekendCoordinaat);

    kopieOndertekendCoordinaat=JSON.parse(JSON.stringify(ondertekendCoordinaat)); // hier wordt een kopie gemaakt van het onderekendCoordinaat. Dat moet omdat we de handtekening er uit moeten halen.
    delete kopieOndertekendCoordinaat.handtekening;
    var controleOnderekendCoordinaatBewijs = JSONCrypt.verifySignature(ondertekendCoordinaat.handtekening, publicKeyKostenBerekenSysteem,kopieOndertekendCoordinaat);
    console.log("Is de handtekening geldig? "+controleOnderekendCoordinaatBewijs);

    //KostenBerekenSysteemTest02
    console.log("KostenBerekenSysteemTest02");

    var eindCoordinaat = {"Lon":"4.9064437","Lat":"52.3736282","tijd":Date.now()};
    var kostenResultaat=kostenBerekenSysteem.berekenKosten(toegangsbewijzen.anoniemToegangsbewijs, ondertekendCoordinaat, eindCoordinaat);
    console.log("kostenResultaat:");
    console.log(kostenResultaat);
    kopieOndertekendCoordinaat=JSON.parse(JSON.stringify(kostenResultaat.ondertekendEindCoordinaat)); // hier wordt een kopie gemaakt van het onderekendCoordinaat. Dat moet omdat we de handtekening er uit moeten halen.
    delete kopieOndertekendCoordinaat.handtekening;
    var controleOndertekendCoordinaatBewijs = JSONCrypt.verifySignature(kostenResultaat.ondertekendEindCoordinaat.handtekening, publicKeyKostenBerekenSysteem,kopieOndertekendCoordinaat);
    console.log("Is de handtekening van de EindCoordinaat geldig? "+controleOndertekendCoordinaatBewijs); 
    kopieKostenSpecificatie=JSON.parse(JSON.stringify(kostenResultaat.kostenSpecificatie)); // hier wordt een kopie gemaakt van het kostenSpecificatie. Dat moet omdat we de handtekening er uit moeten halen.
    delete kopieKostenSpecificatie.handtekening;
    var controleKostenSpecificatie = JSONCrypt.verifySignature(kostenResultaat.kostenSpecificatie.handtekening, publicKeyKostenBerekenSysteem,kopieKostenSpecificatie);
    console.log("Is de handtekening van de kostenSpecificatie geldig? "+controleKostenSpecificatie);

    kopieGebiedsVerklaring=JSON.parse(JSON.stringify(kostenResultaat.gebiedsVerklaring)); // hier wordt een kopie gemaakt van het kostenSpecificatie. Dat moet omdat we de handtekening er uit moeten halen.
    delete kopieGebiedsVerklaring.handtekening;
    var controleGebiedsVerklaring = JSONCrypt.verifySignature(kostenResultaat.gebiedsVerklaring.handtekening, publicKeyKostenBerekenSysteem,kopieGebiedsVerklaring);
    console.log("Is de handtekening van de gebiedsVerklaring geldig? "+controleGebiedsVerklaring);
});

