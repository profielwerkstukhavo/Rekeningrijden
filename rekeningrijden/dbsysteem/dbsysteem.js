const { Level } = require('level');

const persoonlijkeSynchroneEncryptieSleutelDb = new Level('../db/persoonlijkeSynchroneEncryptieSleutels', { valueEncoding: 'json' });
const gebiedsVerklaringenDb = new Level('../db/gebiedsverklaringen', { valueEncoding: 'json' });

// deze module wordt gebruikt om te lezen uit de db en schrijven in de db



exports.put = function (dbNaam, sleutel, inhoud)
{
    if (dbNaam="persoonlijkeSynchroneEncryptieSleutelDb")
    {
        return persoonlijkeSynchroneEncryptieSleutelDb.put(sleutel,inhoud);
    }
    if (dbNaam="gebiedsVerklaringenDb")
    {
        return gebiedsVerklaringenDb.put(sleutel,inhoud);
    }
}

exports.get = function (dbNaam, sleutel)
{
    if (dbNaam="persoonlijkeSynchroneEncryptieSleutelDb")
    {
        return persoonlijkeSynchroneEncryptieSleutelDb.get(sleutel);
    }
    if (dbNaam="gebiedsVerklaringenDb")
    {
        return gebiedsVerklaringenDb.get(sleutel);
    }
}

exports.del = function (dbNaam, sleutel)
{
    if (dbNaam="persoonlijkeSynchroneEncryptieSleutelDb")
    {
        persoonlijkeSynchroneEncryptieSleutelDb.del(sleutel);
    }
    if (dbNaam="gebiedsVerklaringenDb")
    {
        gebiedsVerklaringenDb.del(sleutel);
    }
}

