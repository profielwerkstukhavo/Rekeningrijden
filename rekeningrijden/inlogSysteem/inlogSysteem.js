const fs = require('fs');
const JSONCrypt = require('@jorveld/jsoncrypt');
const dbsysteem = require('../dbsysteem/dbsysteem.js');
const privateKey=fs.readFileSync("../certificaten/privateKeylogin.key");
const publicKey=fs.readFileSync("../certificaten/publicKeylogin.key");
const geldigheidsDuur=60*60*1000; //toetangsbewijs is 60 minuten geldig


exports.login = function (invoer) {
  

    /*
    invoer moet de volgende structuur hebben:
  
    invoer = {
    gebruikersnaam: "",
    wachtwoord: "",
    kenteken: ""
  }

    */
  
  let promise = new Promise(function (resolve, reject) 
  {
    // In deze proef opstelling moet het password altijd 'password' zijn
 
    if(invoer.wachtwoord!="password")
    {
      return "verkeerd password"; // Als er een verkeerd password wordt opgegeven dan wordt een fout teruggegeven.
    }
    
    var anoniemToegangsbewijs = {
      onderekenaar: "Rekening Rijden Nederland",
      geldigtot: Date.now()+geldigheidsDuur
    }
      
    var persoonlijkeSynchroneEncryptieSleutel=JSONCrypt.createSyncEncryptionKeyAndIv();
    // de sleutel en de iv moeten naar een string worden omgezet, omdat ze anders niet goed worden opgeslagen.
    persoonlijkeSynchroneEncryptieSleutel.key=persoonlijkeSynchroneEncryptieSleutel.key.toString('hex');
    persoonlijkeSynchroneEncryptieSleutel.iv=persoonlijkeSynchroneEncryptieSleutel.iv.toString('hex');


    var persoonsGebondenToegangsbewijs = {
      onderekenaar: "Rekening Rijden Nederland",
      uitgaveTijd: Date.now(),
      bsn: "123456789",   // voor het proof of concept wordt altijd hetzelfde BSN gebruikt
      kenteken: invoer.kenteken
    }
      

    var putPromise=dbsysteem.put("persoonlijkeSynchroneEncryptieSleutelDb",invoer.kenteken, persoonlijkeSynchroneEncryptieSleutel);

    // het wegshrijven van de persoonlijkeSynchroneEncryptieSleutel gebeurt niet meteen. Het kan even duren. Daarom moet er een .then worden aangeroepen voor de Promise.
    putPromise.then (function()
    {
      var anoniemToegangsbewijs = {
        onderekenaar: "Rekening Rijden Nederland",
        geldigTot: Date.now()+geldigheidsDuur
      }
    
      signatureAnoniemToegangsbewijs=JSONCrypt.sign(anoniemToegangsbewijs, privateKey);
      anoniemToegangsbewijs.handtekening=signatureAnoniemToegangsbewijs;
      
      signaturePersoonsGebondenToegangsbewijs=JSONCrypt.sign(persoonsGebondenToegangsbewijs,privateKey);
      persoonsGebondenToegangsbewijs.handtekening=signaturePersoonsGebondenToegangsbewijs;
      
      var uitvoer = {
          anoniemToegangsbewijs: anoniemToegangsbewijs,
          persoonsgebondenToegangsbewijs: persoonsGebondenToegangsbewijs,
          persoonlijkeSynchroneEncryptieSleutel: persoonlijkeSynchroneEncryptieSleutel
      } 

      resolve(uitvoer);
    });

  });

  return promise;  

}
