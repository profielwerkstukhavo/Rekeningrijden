const JSONCrypt = require('@jorveld/jsoncrypt');
const inlog = require('./inlogSysteem.js');
const fs = require('fs');

publicKey=fs.readFileSync("../certificaten/publicKeylogin.key");

// test 1
console.log("Testen inlog systeem");
console.log("InlogSysteemTest01");
var logingegevens = 
{
    gebruikersnaam: "Jesper Brandsma",
    wachtwoord: "password",
    kenteken: "AF-93-UT"
}

var inlogPromise=inlog.login(logingegevens);

inlogPromise.then(function(uitvoer)
{
    if (uitvoer=="ERROR")
    {
        console.log("Er is een error opgetreden bij het inloggen");
    }
    kopieVanUitvoer=JSON.parse(JSON.stringify(uitvoer)); // hier wordt een kopie gemaakt van de uitvoer. Dat moet omdat we de handtekening er uit moeten halen.
    
    
    console.log("De toegangsbewijzen die door de inlog module zijn gemaakt");
    console.log(uitvoer);
    delete kopieVanUitvoer.persoonsgebondenToegangsbewijs.handtekening; // hier wordt de handtekening uit de kopie gehaald. Dat is nodig omdat we de hantekening moeten controlen van het toegangsbewijs zonder de handtekening
    delete kopieVanUitvoer.anoniemToegangsbewijs.handtekening;
    var resultaatPersoonsGebondenToegangsBewijs = JSONCrypt.verifySignature(uitvoer.persoonsgebondenToegangsbewijs.handtekening, publicKey,kopieVanUitvoer.persoonsgebondenToegangsbewijs);
    var resultaatAnoniemToegangsbewijs = JSONCrypt.verifySignature(uitvoer.anoniemToegangsbewijs.handtekening, publicKey,kopieVanUitvoer.anoniemToegangsbewijs);
    console.log('Is handtekening PersoonsGebondenToegangsBewijs goed:'+resultaatPersoonsGebondenToegangsBewijs);
    console.log('Is handtekening anoniemToegangsbewijs goed:'+resultaatAnoniemToegangsbewijs);
    
    
    // test 2
    console.log("InlogSysteemTest02");
    var logingegevens = 
    {
        gebruikersnaam: "Jesper Brandsma",
        wachtwoord: "opzettelijk verkeerd password",
        kenteken: "AF-93-UT"
    }
    inlogPromise=inlog.login(logingegevens);

    inlogPromise.then(function(uitvoer)
    {   
        kopieVanUitvoer=JSON.parse(JSON.stringify(uitvoer)); // hier wordt een kopie gemaakt van de uitvoer. Dat moet omdat we de handtekening er uit moeten halen.
        console.log("De toegangsbewijzen die door de inlog module zijn gemaakt");
        console.log(uitvoer);  // moet melding maken van verkeerde password
    });
});