const fs = require('fs');
const JSONCrypt = require('@jorveld/jsoncrypt')
const dbsysteem = require('../dbsysteem/dbsysteem.js');
const opslagsystem = require('../opslagSysteem/opslagSysteem.js');
const publicKeyKostenBerekenSysteem=fs.readFileSync("../certificaten/publicKeyKostenBerekenSysteem.key"); 


exports.controleerKenteken = function (kenteken) {


    var promise = new Promise(function (resolve, reject)
    {
        // Vraagt de persoonlijke synchrone encryptie sleutel

        promisePersoonlijkeEncryptieSleutel=dbsysteem.get("persoonlijkeSynchroneEncryptieSleutelDb",kenteken);
        // de db.get functie geeft een promise terug. Dat komt omdat de functie niet gelijk het resultaat heeft. Iets lezen van disk kost veel tijd. Je moet daar even op wachten.
        // In de functie .then is aangegeven wat er moet gebeuren als het resultaat beschikbaar is.
        promisePersoonlijkeEncryptieSleutel.then(function(persoonlijkeEncryptieKey) 
        {   
            // de key en de iv zijn omgezet in een string omdat ze anders niet kunnen worden opgeslagen. Met onderstaande commandos worden ze weer teruggezet in een buffer.
            var opgehaaldeKey=Buffer.from(persoonlijkeEncryptieKey.key,"hex");
            var opgehaaldeIv=Buffer.from(persoonlijkeEncryptieKey.iv,"hex");

            var cameraJSON= {
                cameraID: 67       // altijd 67 voor deze testopstelling
             }
            var encrypteCameraID=JSONCrypt.cryptSync(cameraJSON, opgehaaldeKey, opgehaaldeIv);
        
            var gebiedVerklaringPromise=opslagsystem.zoekGebiedsVerklaringOp(kenteken,encrypteCameraID);
            gebiedVerklaringPromise.then(function(encrypteGebiedsVerklaring)
            {
                var unencryptedGebiedsVerklaring=JSONCrypt.deCryptSync(encrypteGebiedsVerklaring.encryptedContent,opgehaaldeKey,opgehaaldeIv);
                // controleer de handtekening

                kopieVanUnencryptedGebiedsVerklaring=JSON.parse(JSON.stringify(unencryptedGebiedsVerklaring)); // hier wordt een kopie gemaakt van de unencrypted gebiedsverklaring. Dat moet omdat we de handtekening er uit moeten halen.
                delete kopieVanUnencryptedGebiedsVerklaring.handtekening;

                resolve(JSONCrypt.verifySignature(unencryptedGebiedsVerklaring.handtekening, publicKeyKostenBerekenSysteem, kopieVanUnencryptedGebiedsVerklaring));
            })
        });
    });

    return promise;
}