const JSONCrypt = require('@jorveld/jsoncrypt');
const controleCamera = require('./controleCamera.js');
const inlog = require('../inlogSysteem/inlogSysteem.js');
const kostenBerekenSysteem = require('../kostenBerekenSysteem/kostenBerekenSysteem.js')
const opslagSysteem = require("../opslagSysteem/opslagSysteem.js");

const fs = require('fs');
const publicKeyKostenBerekenSysteem=fs.readFileSync("../certificaten/publicKeyKostenBerekenSysteem.key");

var toegangsbewijzen;


console.log("Testen controleCameraSysteem");


//controleCameraSysteemTest01

console.log("controleCameraSysteemTest01")


// stap 1: log in

var logingegevens = 
{
    gebruikersnaam: "Jesper Brandsma",
    wachtwoord: "password",
    kenteken: "AF-93-UT"
};

var startCoordinaat = {"Lon":"4.9066792","Lat":"52.3735073","tijd":Date.now()};

promiseToegangsbewijzen=inlog.login(logingegevens);
console.log("Promise:");
console.log(promiseToegangsbewijzen);

promiseToegangsbewijzen.then(function (toegangsbewijzen)
{
  console.log("Ingelogd, de volgende toegangsbewijzen zijn aangemaakt:");
  console.log(toegangsbewijzen);

  // stap 2: zetStartCoordinaat
  var ondertekendCoordinaat=kostenBerekenSysteem.zetStartCoordinaat(toegangsbewijzen.anoniemToegangsbewijs,startCoordinaat);
  
  // stap 3: berekenKosten
  var eindCoordinaat = {"Lon":"4.9064437","Lat":"52.3736282","tijd":Date.now()};
  var kostenResultaat=kostenBerekenSysteem.berekenKosten(toegangsbewijzen.anoniemToegangsbewijs, ondertekendCoordinaat, eindCoordinaat);
  console.log("kostenResultaat");
  console.log(kostenResultaat);
  
  // step 4: Versleutel het cameranummer met de synchrone key die in stap 1 is verkregen

  var persoonlijkeEncryptieKey=toegangsbewijzen.persoonlijkeSynchroneEncryptieSleutel;
  // de key en de iv zijn omgezet in een string omdat ze anders niet kunnen worden opgeslagen. Met onderstaande commandos worden ze weer teruggezet in een buffer.
  var persoonlijkeKey=Buffer.from(persoonlijkeEncryptieKey.key,"hex");
  var persoonlijkeIv=Buffer.from(persoonlijkeEncryptieKey.iv,"hex");


  var cameraJSON= {
      cameraID: kostenResultaat.gebiedsVerklaring.cameraID
  }

  console.log("De volgende camera is gevonden:");
  console.log(cameraJSON);

  var encrypteCameraID=JSONCrypt.cryptSync(cameraJSON, persoonlijkeKey, persoonlijkeIv);
  
  console.log("encrypteCameraID:");
  console.log(encrypteCameraID);

  // stap 5: versleutel de de gebiedsverklaring met de synchrone key die in stap 1 is verkregen

  gebiedsVerklaring=kostenResultaat.gebiedsVerklaring;

  var encrypteGebiedsVerklaring=JSONCrypt.cryptSync( gebiedsVerklaring, persoonlijkeKey, persoonlijkeIv);

  console.log("encrypteGebiedsVerklaring:");
  console.log(encrypteGebiedsVerklaring);

  // stap 6: Sla de gebiedsverklaring op (via het opslagsysteem)
  opslagSysteem.slaGebiedsVerklaringOp(toegangsbewijzen.persoonsgebondenToegangsbewijs, encrypteGebiedsVerklaring, encrypteCameraID);

  // stap 7: controleerKenteken
  controleerCameraPromise=controleCamera.controleerKenteken("AF-93-UT");

  controleerCameraPromise.then(function(resultaat) {
    console.log("Resultaat van de controle: "+resultaat);
  });
});



