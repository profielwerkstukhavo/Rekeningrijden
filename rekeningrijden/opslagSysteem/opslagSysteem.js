const fs = require('fs');
const JSONCrypt = require('@jorveld/jsoncrypt');
const dbsysteem = require('../dbsysteem/dbsysteem.js');


const privateKeyKostenBerkenSysteem=fs.readFileSync("../certificaten/privateKeyKostenBerekenSysteem.key");
const publicKeyKostenBerkenSysteem=fs.readFileSync("../certificaten/publicKeyKostenBerekenSysteem.key"); 
const publicKeyLoginSysteem=fs.readFileSync("../certificaten/publicKeylogin.key");


exports.slaGebiedsVerklaringOp = function (persoonlijkToegangsbewijs, encrypteGebiedsVerklaring, encrypteCameraID)
{
    /* invoer:
    persoonlijkToegangsbewijs 
    {
      onderekenaar,
      uitgaveTijd,
      bsn,   
      kenteken,
      handtekening (van het inlogSysteem)
  }

  gebiedsVerklaring = {
    cameraID:0,
    tijd: Date.now()
    handtekening // kostenBerekenSysteem
  }

  */

  // Stap 1 controleer persoonlijkToegangsbewijs
  if (controleerPersoonsGebondenToegangsbewijs(persoonlijkToegangsbewijs)==false)
  {
    return "Het persoonsgebonden toegangsbewijs is niet geldig"; // er is iets mis met het toegangsbewijs. Geef een fout terug.
  }

  // Stap 2 slaat de GebiedsVerklaring op
  return internSlaGebiedsVerklaringOp(persoonlijkToegangsbewijs, encrypteGebiedsVerklaring, encrypteCameraID);
}

exports.zoekGebiedsVerklaringOp = function (kenteken, encrypteCameraID)
{
    // Deze functie kan gebruikt worden door iedereen. Dat is eigenlijk niet goed. Alleen het camera systeem zou gebruik mogen maken van deze functie
    // Omdat dit een prototype is, is dat niet geïmplmenteerd.


    // Stap 1 & 2: Kijk of er een gebiedsbepaling is voor het kenteken en het encrypteComeraID en als dat zo is, geef het terug en verwijder het.
    var encrypteGebiedsVerklaringPromise=internZoekGebiedsVerklaringOp(kenteken, encrypteCameraID);
    return encrypteGebiedsVerklaringPromise;
}

function controleerPersoonsGebondenToegangsbewijs(invoerPersoonsGebondenToegangsbewijs)
{
  var kopiePersoonsGebondenToegangsbewijs=JSON.parse(JSON.stringify(invoerPersoonsGebondenToegangsbewijs)); // hier wordt een kopie gemaakt van het persoonsGebondenToegangsbewijs. Dat moet omdat we de handtekening er uit moeten halen.
  delete kopiePersoonsGebondenToegangsbewijs.handtekening; // hier wordt de handtekening uit de kopie gehaald. Dat is nodig omdat we de hantekening moeten controlen van het toegangsbewijs zonder de handtekening
  resultaatPersoonsGebondenToegangsbewijs = JSONCrypt.verifySignature(invoerPersoonsGebondenToegangsbewijs.handtekening, publicKeyLoginSysteem,kopiePersoonsGebondenToegangsbewijs);
  if (resultaatPersoonsGebondenToegangsbewijs==false)
  {
    return false; // de handtekening op het anonieme toegangsbewijs klopt niet.
  }
  if (invoerPersoonsGebondenToegangsbewijs.onderekenaar!="Rekening Rijden Nederland")
  {
    return false; // het toegangsbewijs is niet uitgegeven door 'Rekening Rijden Nederland'. Het is dus vervalst.
  }
  if (Date.now()>invoerPersoonsGebondenToegangsbewijs.geldigtot)
  {
    return false; // het toegangsbewijs is niet meer geldig. 
  }
  return true; // Alles is goed met het toegangsbewijs.
}


function internSlaGebiedsVerklaringOp(persoonlijkToegangsbewijs, encrypteGebiedsVerklaring, encrypteCameraID)
{
    // Er wordt een oplagsleutel gemaakt die bestaat uit het kenteken + het cameraID (versleuteld). Op die manier kan het de gebiedsverklaring altijd opgezocht worden zonder dat het opslag systeem waar de auto was
    
    var opslagSleutel=persoonlijkToegangsbewijs.kenteken+"-"+encrypteCameraID.encryptedContent;

    // console.log("Sla op met sleutel:"+opslagSleutel);

    return dbsysteem.put("gebiedsVerklaringenDb", opslagSleutel, encrypteGebiedsVerklaring);
}

async function internZoekGebiedsVerklaringOp(kenteken, encrypteCameraID)
{

    var opslagSleutel=kenteken+"-"+encrypteCameraID.encryptedContent;
    var encrypteGebiedsVerklaringPromise;
    
    encrypteGebiedsVerklaringPromise=dbsysteem.get("gebiedsVerklaringenDb", opslagSleutel);
    
    dbsysteem.del("gebiedsVerklaringenDb", opslagSleutel);

    // console.log("Zoek op met sleutel:"+opslagSleutel);
    return encrypteGebiedsVerklaringPromise;
}
