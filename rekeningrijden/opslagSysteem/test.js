const JSONCrypt = require('@jorveld/jsoncrypt');
const kostenBerekenSysteem = require('../kostenBerekenSysteem/kostenBerekenSysteem.js');
const inlog = require('../inlogSysteem/inlogSysteem.js');
const fs = require('fs');
const opslagSysteem = require('./opslagSysteem.js');


const publicKeyKostenBerekenSysteem=fs.readFileSync("../certificaten/publicKeyKostenBerekenSysteem.key"); 

console.log("Testen opslagSysteem systeem");

//KostenBerekenSysteemTest01
console.log("OpslagSysteemTest01");

var logingegevens = 
{
    gebruikersnaam: "Jesper Brandsma",
    wachtwoord: "password",
    kenteken: "AF-93-UT"
};

var startCoordinaat = {"Lon":"4.9066792","Lat":"52.3735073","tijd":Date.now()};

var loginPromise=inlog.login(logingegevens);

loginPromise.then(function(toegangsbewijzen)
{


    ondertekendCoordinaat=kostenBerekenSysteem.zetStartCoordinaat(toegangsbewijzen.anoniemToegangsbewijs,startCoordinaat);
    var eindCoordinaat = {"Lon":"4.9064437","Lat":"52.3736282","tijd":Date.now()};
    var kostenResultaat=kostenBerekenSysteem.berekenKosten(toegangsbewijzen.anoniemToegangsbewijs, ondertekendCoordinaat, eindCoordinaat);



    console.log("toegangsbewijzen");
    console.log(toegangsbewijzen);
    console.log("ondertekendCoordinaat:");
    console.log(ondertekendCoordinaat);
    console.log("kostenResultaat:");
    console.log(kostenResultaat);

    var opgehaaldeKey=Buffer.from(toegangsbewijzen.persoonlijkeSynchroneEncryptieSleutel.key,"hex");
    var opgehaaldeIv=Buffer.from(toegangsbewijzen.persoonlijkeSynchroneEncryptieSleutel.iv,"hex");


    encrypteGebiedsVerklaring=JSONCrypt.cryptSync(kostenResultaat.gebiedsVerklaring, opgehaaldeKey, opgehaaldeIv);
    console.log("encrypteGebiedsVerklaring:");
    console.log(encrypteGebiedsVerklaring);
    console.log("decrypted GebiedsVerklaring:");
    console.log(JSONCrypt.deCryptSync(encrypteGebiedsVerklaring.encryptedContent, opgehaaldeKey, opgehaaldeIv));

    encrypteGebiedsVerklaring=JSONCrypt.cryptSync(kostenResultaat.gebiedsVerklaring, opgehaaldeKey, opgehaaldeIv);

    var cameraJSON= {
        cameraID: kostenResultaat.gebiedsVerklaring.cameraID
    }

    encrypteCameraID=JSONCrypt.cryptSync(cameraJSON, opgehaaldeKey, opgehaaldeIv);

    console.log("encrypteCameraID:");
    console.log(encrypteCameraID);
    console.log("decrypted CameraID:");
    console.log(JSONCrypt.deCryptSync(encrypteCameraID.encryptedContent, opgehaaldeKey, opgehaaldeIv));


    resultaatPromise=opslagSysteem.slaGebiedsVerklaringOp(toegangsbewijzen.persoonsgebondenToegangsbewijs, encrypteGebiedsVerklaring, encrypteCameraID);
    // het oplsaan van de gebiedsverklaring kan even duren. Daarom wordt er een promise teruggegeven. Met promise.then() kun je code uitvoeren als de opslag klaar is.
    resultaatPromise.then(function(result) 
    {
        console.log("OpslagSysteemTest02");
        // OpslagSysteemTest02

        var resultaatZoekGebiedsVerklaringPromise=opslagSysteem.zoekGebiedsVerklaringOp(toegangsbewijzen.persoonsgebondenToegangsbewijs.kenteken, "iets dat niet bestaat");
    
        // zoekGebiedsVerklaringOp is een asynchrone functie. Dat betekent dat de functie een 'promise' teruggeeft. De waarde van de promise is niet gelijk beschikbaar.
        // Dat gebeurt pas als het record is opgezocht (en verwijderd). Daarom moet een promise.then() gebruikt worden zodat de code wacht tot het resultaat beschikbaar is.
        resultaatZoekGebiedsVerklaringPromise.then(function(result) 
        {
            console.log("resultaatZoekGebiedsVerklaring: ");
            console.log(result)
        }).catch(function(error)
        {
            // als er geen gebiedsverklaring gevonden wordt, dan wordt onderstaande error op het scherm gezet.
            console.log("Er is geen gebiedsverklaring gevonden.");
            //OpslagSysteemTest03
            console.log("OpslagSysteemTest03");
            resultaatZoekGebiedsVerklaring=opslagSysteem.zoekGebiedsVerklaringOp(toegangsbewijzen.persoonsgebondenToegangsbewijs.kenteken, encrypteCameraID);
            resultaatZoekGebiedsVerklaring.then(function(result) 
            {
                console.log("OpslagSysteemTest03");
                console.log("resultaatZoekGebiedsVerklaring: ");
                console.log(result)
                console.log("decrypted GebiedsVerklaring:");
                console.log(JSONCrypt.deCryptSync(result.encryptedContent, opgehaaldeKey, opgehaaldeIv));
            })
         })
    });
});


